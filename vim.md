**VIM commands:**
-------------------------------------------------------------------------
-------------------------------------------------------------------------
Modes for VIM editor
-------------------------------------------------------------------------
* Visual
* Insert
* Command

-------------------------------------------------------------------------

Entering into VIM editor:
--> Press 'I' or 'i' to enter into INSERT mode.
--> Edit the file.
--> Press ESC which exists the INSERT mode, you are now in COMMAND mode.
--> Type -> :wq (write and quit).
--> Your files are save and exited from the editor.
