
**Basic commands to get started:**
-------------------------------------------------------------------------
-------------------------------------------------------------------------

Check version or availability

--> git --version (check your current version of Git)

-------------------------------------------------------------------------
Set your username:

--> git config --global user.name "FIRST_NAME LAST_NAME"

Set your email address:

--> git config --global user.email "MY_NAME@example.com"

Check username and email address

--> git config --global --list

-------------------------------------------------------------------------

**Working with local machine:**
-------------------------------------------------------------------------
-------------------------------------------------------------------------

Display the state of the working directory and the staging area

--> git status

-------------------------------------------------------------------------

Creating a local repository

--> git init (Creates a local repository).

-------------------------------------------------------------------------

Create a file:

--> touch sample.py (creates a file for you which is UNTRACKED)

--> git add . or git add sample.py (Git starts TRACKING the file)

--> git commit -m 'added an empty file' (Git puts the file into STAGGING area).

-------------------------------------------------------------------------
Modifying an existing file:

--> Edit the existing file and save it.

--> git add . (GIT is TRACKING a modified file.)

--> git commit -m 'modified the file'

-------------------------------------------------------------------------
Selects that file, and moves it to the staging area, marking it for inclusion in the next commit

--> git add <file name> (stage a specific file or directory)

--> git add . (stage all files)

