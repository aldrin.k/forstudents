**RemoteMachine Commands to use Git.**

-------------------------------------------------------------------------
-------------------------------------------------------------------------

Clone a remote repository

--> git clone URL (Enter the URL you want to clone from)

-------------------------------------------------------------------------
Move inside into local repository 

--> cd project_name 

(edit whatever changes needed)
-------------------------------------------------------------------------
Work in the repository

--> git commit -a -m 'modified the file' (Skipping the stagging area)

-------------------------------------------------------------------------

Upload local repository content to a remote repository 

--> git push -u "URL" <branch> (for initial push to remote repository)

--> git push (once your local repository is connected with to remote repo)

--------------------------------------------------------------------------

Update the local version of a repository from a remote

--> git pull "URL" 

--------------------------------------------------------------------------

Utility tool to review and read a history of everything that happens to a repository

--> git log

--------------------------------------------------------------------------

List all of the branches in your repository

--> git branch or git branch --list 